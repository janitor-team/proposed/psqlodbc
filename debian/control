Source: psqlodbc
Priority: optional
Section: database
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders: Christoph Berg <myon@debian.org>, Steve Langasek <vorlon@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libpq-dev,
 postgresql,
 postgresql-common (>= 144~),
 postgresql-server-dev-all,
 unixodbc-dev (>= 2.2.11-13),
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://odbc.postgresql.org/
Vcs-Git: https://salsa.debian.org/postgresql/psqlodbc.git
Vcs-Browser: https://salsa.debian.org/postgresql/psqlodbc

Package: odbc-postgresql
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Enhances: unixodbc
Breaks:
 libiodbc2 (<= 3.52.7-2),
 odbcinst1debian2 (<< 2.2.14p2-3),
 unixodbc (<< 2.2.14p2-3),
Description: ODBC driver for PostgreSQL
 This package provides a driver that allows ODBC-enabled applications to
 access PostgreSQL databases.  ODBC is an abstraction layer that allows
 applications written for that layer to access databases in a manner
 that is relatively independent of the particular database management
 system.
 .
 You need to install this package if you want to use an application that
 provides database access through ODBC and you want that application to
 access a PostgreSQL database.  This package would need to be installed
 on the same machine as that client application; the PostgreSQL database
 server can be on a different machine and does not need any additional
 software to accept ODBC clients.
 .
 If you want to write software that can access a database through the
 ODBC abstraction layer, you need to install the unixODBC driver manager
 development package unixodbc-dev, and possibly additional packages for
 language bindings.  This driver package is only used at run time.
